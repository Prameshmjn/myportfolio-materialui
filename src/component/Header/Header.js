import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import IconButton from "@material-ui/core/IconButton";
import Typography from "@material-ui/core/Typography";
import MenuItem from "@material-ui/core/MenuItem";
import Menu from "@material-ui/core/Menu";
import MenuIcon from "@material-ui/icons/Menu";
import "./Header.css";
import { Facebook, GitHub, Home, Instagram, Send } from "@material-ui/icons";
import { CustomButton } from "../Button/Button";
import { Link, NavLink } from "react-router-dom";

const useStyles = makeStyles((theme) => ({
  grow: {
    flexGrow: 1,
  },
  menuButton: {
    marginRight: theme.spacing(2),
    background: "var(--main-color)",
  },
  title: {
    display: "none",
    paddingTop: "19px",
    margin: "0 10px",
    fontSize: "18px",

    [theme.breakpoints.up("sm")]: {
      display: "block",
    },
  },
  link: {
    textDecoration: "none",
    color: "#000",
    textTransform: "uppercase",

    "&:hover": {
      color: "#999",
    },
  },
  linkbtn: {
    textDecoration: "none",
    color: "#000",
    display: "flex",
  },
  link_para: {
    fontSize: "15px",
    padding: "5px",
  },
  inputRoot: {
    color: "inherit",
  },
  sectionDesktop: {
    display: "none",
    [theme.breakpoints.up("lg")]: {
      display: "flex",
    },
  },
  sectionMobile: {
    display: "flex",
    [theme.breakpoints.up("lg")]: {
      display: "none",
    },
  },
}));

export default function PrimarySearchAppBar() {
  const classes = useStyles();
  const [anchorEl, setAnchorEl] = React.useState(null);
  const [mobileMoreAnchorEl, setMobileMoreAnchorEl] = React.useState(null);

  const isMenuOpen = Boolean(anchorEl);
  const isMobileMenuOpen = Boolean(mobileMoreAnchorEl);

  const handleMobileMenuClose = () => {
    setMobileMoreAnchorEl(null);
  };

  const handleMenuClose = () => {
    setAnchorEl(null);
    handleMobileMenuClose();
  };

  const handleMobileMenuOpen = (event) => {
    setMobileMoreAnchorEl(event.currentTarget);
  };

  const menuId = "primary-search-account-menu";
  const renderMenu = (
    <Menu
      anchorEl={anchorEl}
      anchorOrigin={{ vertical: "top", horizontal: "right" }}
      id={menuId}
      keepMounted
      transformOrigin={{ vertical: "top", horizontal: "right" }}
      open={isMenuOpen}
      onClose={handleMenuClose}
    >
      <MenuItem onClick={handleMenuClose}>Profile</MenuItem>
      <MenuItem onClick={handleMenuClose}>My account</MenuItem>
    </Menu>
  );

  const mobileMenuId = "primary-search-account-menu-mobile";
  const renderMobileMenu = (
    <Menu
      anchorEl={mobileMoreAnchorEl}
      anchorOrigin={{ vertical: "top", horizontal: "right" }}
      id={mobileMenuId}
      keepMounted
      transformOrigin={{ vertical: "top", horizontal: "right" }}
      open={isMobileMenuOpen}
      onClose={handleMobileMenuClose}
    >
      <MenuItem>
        <NavLink to="/resume" className={classes.linkbtn}>
          <p className={classes.link_para}>Resume</p>
        </NavLink>
      </MenuItem>
      <MenuItem>
        <NavLink to="/portfolio" className={classes.linkbtn}>
          <p className={classes.link_para}>Portfolio</p>
        </NavLink>
      </MenuItem>
    </Menu>
  );

  return (
    <div>
      <div className={classes.grow}>
        <AppBar className={"navbar"} position="static">
          <Toolbar>
            <IconButton
              edge="start"
              className={classes.menuButton}
              color="inherit"
            >
              <Link to="/">
                <Home className="homeIcon" />
              </Link>
            </IconButton>
            <Typography className={classes.title} variant="h6" noWrap>
              <Link to="/" className={classes.link}>
                Resume
              </Link>
            </Typography>
            <Typography className={classes.title} variant="h6" noWrap>
              <Link to="/portfolio" className={classes.link}>
                Portfolio
              </Link>
            </Typography>

            <div className={classes.grow} />
            <div className={classes.sectionDesktop}>
              <IconButton color="inherit">
                <NavLink
                  to="//www.facebook.com/prameshmankiller"
                  target="__blank"
                  className={classes.linkbtn}
                >
                  <Facebook />
                </NavLink>
              </IconButton>
              <IconButton color="inherit">
                <NavLink
                  to="//www.facebook.com/prameshmankiller"
                  target="__blank"
                  className={classes.linkbtn}
                >
                  <Instagram />
                </NavLink>
              </IconButton>
              <IconButton color="inherit">
                <NavLink
                  to="//www.facebook.com/prameshmankiller"
                  target="__blank"
                  className={classes.linkbtn}
                >
                  <GitHub />
                </NavLink>
              </IconButton>
            </div>
            <div className={classes.sectionMobile}>
              <IconButton
                aria-label="show more"
                aria-controls={mobileMenuId}
                aria-haspopup="true"
                onClick={handleMobileMenuOpen}
                color="inherit"
              >
                <MenuIcon />
              </IconButton>
            </div>
            <div className="nav_btn">
              <CustomButton
                text={"Hire Me"}
                icon={<Send className="icon_download" />}
              />
            </div>
          </Toolbar>
        </AppBar>
        {renderMobileMenu}
        {renderMenu}
      </div>
    </div>
  );
}
