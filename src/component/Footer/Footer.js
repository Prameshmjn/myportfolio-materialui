import { Typography } from "@material-ui/core";
import { Copyright } from "@material-ui/icons";
import React from "react";
import "./Footer.css";

const Footer = () => {
  return (
    <div className="footer">
      <div className="footer_left">
        <Typography className="footer_name">Pramesh Maharjan</Typography>
      </div>
      <div className="footer_right">
        <Copyright />
        <Typography className="footer_copyright">
          All Rights Reserved.{" "}
          <span>
            Designed By <span> Pramesh</span>
          </span>
        </Typography>
      </div>
    </div>
  );
};

export default Footer;
