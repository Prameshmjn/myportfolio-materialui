import { Container, Grid, Typography } from "@material-ui/core";
import { useLayoutEffect } from "react";
import "./Skill.css";

export const Skill = () => {
  useLayoutEffect(() => {
    const skillsSection = document.getElementById("skills-section");

    const progressBars = document.querySelectorAll(".progress-bar");
    const showProgress = () => {
      progressBars.forEach((progressBar) => {
        const value = progressBar.dataset.progress;
        progressBar.style.opacity = 1;
        progressBar.style.width = `${value}%`;
      });
    };
    const hideProgress = () => {
      progressBars.forEach((p) => {
        p.style.opacity = 0;
        p.style.width = 0;
      });
    };
    window.addEventListener("scroll", () => {
      const sectionPos = skillsSection.getBoundingClientRect().top;
      const screenPos = window.innerHeight / 2;
      if (sectionPos < screenPos) {
        showProgress();
      } else {
        hideProgress();
      }
    });
  }, []);

  return (
    <Container className="skill_container">
      <Grid item>
        <Typography variant="h6" className="Skill_title">
          Skills
        </Typography>
      </Grid>
      <Grid item>
        <div id="skills-section">
          <div className="container">
            <p>HTML</p>
            <div className="progress">
              <div className="progress-bar" data-progress="90">
                <span>90%</span>
              </div>
            </div>

            <p>CSS</p>
            <div className="progress">
              <div className="progress-bar" data-progress="80">
                <span>80%</span>
              </div>
            </div>

            <p>JS</p>
            <div className="progress">
              <div className="progress-bar" data-progress="70">
                <span>70%</span>
              </div>
            </div>

            <p>REACT</p>
            <div className="progress">
              <div className="progress-bar" data-progress="80">
                <span>80%</span>
              </div>
            </div>

            <p>NODE</p>
            <div className="progress">
              <div className="progress-bar" data-progress="75">
                <span>75%</span>
              </div>
            </div>
          </div>
        </div>
      </Grid>
    </Container>
  );
};
