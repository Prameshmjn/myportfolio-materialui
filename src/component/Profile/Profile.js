import { Card, Typography } from "@material-ui/core";
import React from "react";
import { CustomTimeline, CustomTimelineSeperator } from "../Timeline/Timeline";

import { resumeData } from "../../utils/resumeData";

import "./Profile.css";
import { GetApp, PersonOutlined } from "@material-ui/icons";
import { TimelineContent, TimelineItem } from "@material-ui/lab";
import { CustomButton } from "../Button/Button";

export const CustomTimelineItem = ({ title, text, link }) => {
  return (
    <TimelineItem>
      <CustomTimelineSeperator />
      <TimelineContent className={"timeline_content"}>
        {link ? (
          <Typography className={"timelineItem_text"}>
            <span>{title}: </span>
            <a href={link} target="__blank">
              {text}
            </a>
          </Typography>
        ) : (
          <Typography className={"timelineItem_text"}>
            <span>{title}:</span>
            {text}
          </Typography>
        )}
      </TimelineContent>
    </TimelineItem>
  );
};
export const CustomTimelineItem2 = ({ title, text, des }) => {
  return (
    <TimelineItem>
      <CustomTimelineSeperator />
      <TimelineContent className={"timeline_content2"}>
        <Typography className={"timelineItem_text2"}>
          <span className={"spaceintimeline"}>{title}</span>
          <small className={"spaceintimeline"}>{text}</small>
          <small className={"spaceintimeline"}>{des}</small>
        </Typography>
      </TimelineContent>
    </TimelineItem>
  );
};

export const Profile = () => {
  return (
    <Card elevation={6}>
      <div className="profile container_shadow">
        <div className="profile_name">
          <Typography className="name">{resumeData.Name}</Typography>
          <Typography className="title">{resumeData.Title}</Typography>
        </div>
        <figure className="profile_image">
          <img src="./images/bgbg.jpg" alt="" />
        </figure>
        <div className="profile_information">
          <CustomTimeline icon={<PersonOutlined />}>
            <CustomTimelineItem title="Name" text={resumeData.Name} />
            <CustomTimelineItem title="Title" text={resumeData.Title} />
            <CustomTimelineItem title="Email" text={resumeData.Email} />
            {Object.keys(resumeData.social).map((key, index) => (
              <CustomTimelineItem
                key={index}
                title={key}
                text={resumeData.social[key].text}
                link={resumeData.social[key].link}
              />
            ))}
            <div className={"make_btn"}>
              <CustomButton
                icon={<GetApp className="icon_download" />}
                text={"Download Cv"}
                onClick={() => console.log("clicked")}
              />
            </div>
          </CustomTimeline>
        </div>
      </div>
    </Card>
  );
};
