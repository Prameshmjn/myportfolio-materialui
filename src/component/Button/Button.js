import { Button } from "@material-ui/core";
import React from "react";
import "./Button.css";

export const CustomButton = ({ icon, text }) => {
  return (
    <Button
      className="custom_btn"
      endIcon={icon ? <div className="icon_container">{icon}</div> : null}
    >
      {text === "Download Cv" ? (
        <a
          href={process.env.PUBLIC_URL + "/prameshcv.doc"}
          download
          target="__blank"
        >
          <span className="btn_text">{text}</span>
        </a>
      ) : null}
      {text === "Hire Me" ? (
        <a href="/#contact">
          <span className="btn_text">{text}</span>
        </a>
      ) : null}
    </Button>
  );
};
