import { Typography } from "@material-ui/core";
import {
  TimelineConnector,
  TimelineDot,
  TimelineItem,
  TimelineSeparator,
  Timeline,
  TimelineContent,
} from "@material-ui/lab";
import React from "react";
import "./Timeline.css";

export const CustomTimeline = ({ title, children, icon }) => {
  return (
    <Timeline className={"timeline"}>
      {/* timeline headser */}
      <TimelineItem className={"timeline_firstItem"}>
        <TimelineSeparator>
          <TimelineDot className={"timeline_dot_header"}>{icon}</TimelineDot>
          <TimelineConnector />
        </TimelineSeparator>
        <TimelineContent>
          <Typography variant="h6" className={"timeline_header"}>
            {title}
          </Typography>
        </TimelineContent>
      </TimelineItem>
      {children}
      {/* timeline body */}
    </Timeline>
  );
};
export const CustomTimelineSeperator = () => {
  return (
    <TimelineSeparator className={"seperator_padding"}>
      <TimelineDot variant={"outlined"} className={"timeline_dot"} />
      <TimelineConnector />
    </TimelineSeparator>
  );
};
