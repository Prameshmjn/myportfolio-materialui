import { Facebook, GitHub, Instagram } from "@material-ui/icons";

export const portfolioData = [
  {
    tag: "React",
    caption: "A short description",
    image: process.env.PUBLIC_URL + "/images/service2.png",
    title: "Project 1",
    description: "this is my project description",
    links: [
      {
        link: "http://www.facebook.com",
        icon: <Facebook />,
      },
      {
        link: "http://www.facebook.com",
        icon: <Instagram />,
      },
      {
        link: "http://www.facebook.com",
        icon: <GitHub />,
      },
    ],
  },
  {
    tag: "React",
    caption: "A short description",
    image: process.env.PUBLIC_URL + "/images/service1.jpg",
    title: "Project 1",
    description: "this is my project description",
    links: [
      {
        link: "http://www.facebook.com",
        icon: <Facebook />,
      },
      {
        link: "http://www.facebook.com",
        icon: <Instagram />,
      },
      {
        link: "http://www.facebook.com",
        icon: <GitHub />,
      },
    ],
  },
  {
    tag: "Java",
    caption: "A short description",
    image: process.env.PUBLIC_URL + "/images/service2.png",
    title: "Project 1",
    description: "this is my project description",
    links: [
      {
        link: "http://www.facebook.com",
        icon: <Facebook />,
      },
      {
        link: "http://www.facebook.com",
        icon: <Instagram />,
      },
      {
        link: "http://www.facebook.com",
        icon: <GitHub />,
      },
    ],
  },
  {
    tag: "Javascript",
    caption: "A short description",
    image: process.env.PUBLIC_URL + "/images/service3.png",
    title: "Project 1",
    description: "this is my project description",
    links: [
      {
        link: "http://www.facebook.com",
        icon: <Facebook />,
      },
      {
        link: "http://www.facebook.com",
        icon: <Instagram />,
      },
      {
        link: "http://www.facebook.com",
        icon: <GitHub />,
      },
    ],
  },
];
