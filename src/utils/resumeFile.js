export const work = [
  {
    title: "Web Development Course",
    subTitle: "2021 - Present",
    des: "I have completed my webDevelopment course from BroadWay Infosys.",
  },
  {
    title: "MERN Course",
    subTitle: "2021 - Present",
    des: "I have completed my MERN course from BroadWay Infosys",
  },
  {
    title: "Materail Ui",
    subTitle: "2021- Present",
    des: "I have been mastering Material UI for react for 1 weeks",
  },
];

export const education = [
  {
    title: "Bachelor in Electronics and Communication Engineering",
    subTitle: "2016 - Present",
    des: "I have been studying electronics and communication engineering in Kathmandu Engineering College,kalimati",
  },
  {
    title: "Science and Mathematics",
    subTitle: "2014 - 2016",
    des: "Completedd my Higher Secondary School from Everest Higher Secondary School,Kalimati,Kathmandu",
  },
  {
    title: "S.L.C",
    subTitle: "2012 - 2014",
    des: "Completed my S.L.C from Shree Aadinath Secondary School,Chobhar,Kirtipur",
  },
];
