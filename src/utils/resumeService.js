export const service = [
  {
    title: "FrontEnd Development",
    description: "i have been developing web for 5 months",
    image: process.env.PUBLIC_URL + "/images/service2.png",
  },
  {
    title: "BackEnd Development",
    description: "i have been developing web for 5 months",
    image: process.env.PUBLIC_URL + "/images/service3.png",
  },
  {
    title: "Web Development",
    description: "i have been developing web for 5 months",
    image: process.env.PUBLIC_URL + "/images/service1.jpg",
  },
];
