import { Facebook, GitHub, Instagram, LinkedIn } from "@material-ui/icons";

export const resumeData = {
  Name: "Pramesh Maharjan",
  Title: "FullStacks developer",

  Birthday: "1996-10-14",
  Job: "MERN DEVELOPER",
  Email: "prameshmjn@gmail.com",
  Address: "kirtipu,kathmandu",
  Phone: "9808052921",

  social: {
    Facebook: {
      link: "http://facebook.com/prameshmankiller",
      text: "Facebook",
      Icon: <Facebook />,
    },
    LinkedIn: {
      link: "http://facebook.com/prameshmankiller",
      text: "LinkedIn",
      Icon: <LinkedIn />,
    },
    Instagram: {
      link: "http://facebook.com/prameshmankiller",
      text: "Instagram",
      Icon: <Instagram />,
    },
    GitHub: {
      link: "http://facebook.com/prameshmankiller",
      text: "Github",
      Icon: <GitHub />,
    },
  },
};
