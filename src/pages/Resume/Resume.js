import {
  Button,
  Card,
  CardActionArea,
  CardContent,
  CardMedia,
  Container,
  Grid,
  IconButton,
  makeStyles,
  TextField,
  Typography,
} from "@material-ui/core";
import { Facebook, GitHub, Instagram, Work } from "@material-ui/icons";
import React from "react";
import { CustomTimelineItem2 } from "../../component/Profile/Profile";
import { CustomTimeline } from "../../component/Timeline/Timeline";
import { education, work } from "../../utils/resumeFile";
import { service } from "../../utils/resumeService";
import "./Resume.css";
import { Skill } from "../../component/Skill/Skill";
import { NavLink } from "react-router-dom";

const useStyles = makeStyles({
  aboutBody: {
    letterSpacing: "1px",
    fontWeight: 500,
  },
});

const Resume = () => {
  const classes = useStyles();
  return (
    <div className="about_me ">
      <Grid>
        {/* -----AboutMe---- */}
        <Grid item>
          <div className="aboutme">
            <Typography variant="h6" gutterBottom>
              About Me
            </Typography>
            <Typography variant="body1" className={classes.aboutBody}>
              I am a web developer who puts his soul out while developing web
              applications.I have completed my Web development course and
              wanting to work in a prestigous company
              <br />
              <br />I am an engineer who likes to enjoy developing web
              application.I am studying Electronics Engineering
            </Typography>
          </div>
        </Grid>
        {/* -------TimeLine----- */}
        <Grid item>
          <div className="experience_head" data-aos="fade-right">
            <Typography variant="h6" gutterBottom>
              Qualifications
            </Typography>
            <div className="experience">
              <div>
                <CustomTimeline icon={<Work />} title={"Working History"}>
                  {work.map((item, index) => (
                    <CustomTimelineItem2
                      key={index}
                      title={item.title}
                      text={item.subTitle}
                      des={item.des}
                    />
                  ))}
                </CustomTimeline>
              </div>
              <div>
                <CustomTimeline
                  icon={<i className="fas fa-graduation-cap cap"></i>}
                  title="Educational Background"
                >
                  {education.map((item, index) => (
                    <CustomTimelineItem2
                      key={index}
                      title={item.title}
                      text={item.subTitle}
                      des={item.des}
                    />
                  ))}
                </CustomTimeline>
              </div>
            </div>
          </div>
        </Grid>
        {/* --------Services Start------ */}
        <Grid item>
          <Container className={"mainroot1"}>
            <Typography variant="h6" className={"service_title"}>
              My Services
            </Typography>
            <div className="service_card_main">
              {service.map((item, index) => (
                <Card
                  key={index}
                  className={"service_card"}
                  data-aos="fade--up"
                >
                  <CardActionArea>
                    <CardMedia
                      className="service_card_image"
                      image={item.image}
                    />

                    <CardContent className="service_card_content">
                      <Typography gutterBottom variant="h6" component="h2">
                        {item.title}
                      </Typography>
                      <Typography variant="body2" component="p">
                        {item.description}
                      </Typography>
                    </CardContent>
                  </CardActionArea>
                </Card>
              ))}
            </div>
          </Container>
        </Grid>
        {/* ------skill------ */}
        <Grid item>
          <Skill />
        </Grid>
        {/* -------contact-form----- */}
        <Grid item xs={12} id="contact">
          <div className="contact_main">
            <Typography variant="h6" className="contact_title">
              Contact Me
            </Typography>
            <div className="contact_container" sm={12}>
              <div className="contact_left">
                <Typography variant="h6" className="contact_left_title">
                  Contact Form
                </Typography>
                <form action="https://formspree.io/f/xgerkoqz" method="post">
                  <TextField
                    variant="standard"
                    label="Name"
                    name="name"
                    className="contact_fields"
                  ></TextField>
                  <TextField
                    variant="standard"
                    label="Email"
                    name="email"
                    name="_replyto"
                    className="contact_fields"
                  ></TextField>
                  <br />
                  <TextField
                    variant="standard"
                    label="Message"
                    name="message"
                    multiline
                    rows={3}
                    fullWidth
                    className="contact_fields"
                  ></TextField>
                  <Button
                    variant="contained"
                    type="submit"
                    value="Send"
                    className="contact_btn"
                  >
                    Submit
                  </Button>
                </form>
              </div>
              <div className="contact_right">
                <Typography variant="h6" className="contact_right_title">
                  Contact Information
                </Typography>
                <Typography variant="body1" className="contact_right_content">
                  Address:<small>Chobhar,Kirtipur,Kathmandu,Nepal</small>
                </Typography>
                <Typography variant="body1" className="contact_right_content">
                  Phone:<small>+977 9808052921</small>
                </Typography>
                <Typography variant="body1" className="contact_right_content">
                  Job:<small>Web Developer</small>
                </Typography>
                <Typography variant="body1" className="contact_right_content">
                  Email:<small>prameshmjn@gmail.com</small>
                </Typography>

                <IconButton color="inherit">
                  <NavLink
                    to="//www.facebook.com/prameshmankiller"
                    target="__blank"
                    className="contact_icons"
                  >
                    <Facebook />
                  </NavLink>
                </IconButton>
                <IconButton color="inherit">
                  <NavLink
                    to="//www.facebook.com/prameshmankiller"
                    target="__blank"
                    className="contact_icons"
                  >
                    <Instagram />
                  </NavLink>
                </IconButton>
                <IconButton color="inherit">
                  <NavLink
                    to="//www.facebook.com/prameshmankiller"
                    target="__blank"
                    className="contact_icons"
                  >
                    <GitHub />
                  </NavLink>
                </IconButton>
              </div>
            </div>
          </div>
        </Grid>
      </Grid>
    </div>
  );
};

export default Resume;
