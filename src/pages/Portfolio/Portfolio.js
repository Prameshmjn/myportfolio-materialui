import {
  Card,
  CardActionArea,
  CardContent,
  CardMedia,
  Dialog,
  DialogActions,
  DialogContent,
  DialogTitle,
  Grid,
  Grow,
  Tab,
  Tabs,
  Typography,
} from "@material-ui/core";
import React, { useState } from "react";
import { portfolioData } from "../../utils/portfolioData";
import "./portfolio.css";

const Portfolio = () => {
  const [tabValue, setTabValue] = useState("All");
  const [projectDialog, setProjectDialog] = useState(false);

  return (
    <div className="portfolio_main">
      <Grid>
        <div className="portfoliomain">
          <Typography variant="h6" gutterBottom>
            Portfolio
          </Typography>
        </div>
        <Grid item>
          <Tabs
            value={tabValue}
            className="custom_tabs"
            onChange={(event, newValue) => setTabValue(newValue)}
          >
            <Tab
              label="All"
              value="All"
              className={
                tabValue === "All"
                  ? "customTabs_item active"
                  : "customTabs_item"
              }
            />
            {[...new Set(portfolioData.map((item) => item.tag))].map(
              (item, index) => (
                <Tab
                  key={index}
                  label={item}
                  value={item}
                  className={
                    tabValue === item
                      ? "customTabs_item active"
                      : "customTabs_item"
                  }
                ></Tab>
              )
            )}
          </Tabs>
        </Grid>
        <Grid item xs={12}>
          <Grid container>
            {portfolioData.map((projects, index) => (
              <React.Fragment key={index}>
                {tabValue === projects.tag || tabValue === "All" ? (
                  <Grid item className="card_container">
                    <Grow in timeout={1000}>
                      <Card
                        className="custom_card"
                        onClick={() => setProjectDialog(projects)}
                      >
                        <CardActionArea>
                          <CardMedia
                            className="custom_card_image"
                            image={projects.image}
                          ></CardMedia>
                          <CardContent>
                            <Typography variant="body2">
                              {projects.title}
                            </Typography>
                            <Typography variant="body2" className="caption">
                              {projects.caption}
                            </Typography>
                          </CardContent>
                        </CardActionArea>
                      </Card>
                    </Grow>
                  </Grid>
                ) : null}
              </React.Fragment>
            ))}
          </Grid>
        </Grid>
        <Dialog open={projectDialog} onClose={() => setProjectDialog(false)}>
          <DialogTitle onClose={() => setProjectDialog(false)}>
            {projectDialog.title}
          </DialogTitle>
          <img src="" alt="" className="projectDialog_image" />
          <DialogContent>
            <Typography className="projectDialog_description">
              {projectDialog.description}
            </Typography>
          </DialogContent>
          <DialogActions className="projectDialog_actions">
            {projectDialog &&
              projectDialog.links.map((link) => (
                <a
                  href={link.link}
                  target="__blank"
                  className="projectDialog_icons"
                >
                  {link.icon}
                </a>
              ))}
          </DialogActions>
        </Dialog>
        ;
      </Grid>
    </div>
  );
};

export default Portfolio;
