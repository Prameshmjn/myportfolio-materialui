import {
  Container,
  createMuiTheme,
  Grid,
  MuiThemeProvider,
  ThemeProvider,
} from "@material-ui/core";
import Footer from "./component/Footer/Footer";
import Header from "./component/Header/Header";
import { Profile } from "./component/Profile/Profile";
import Portfolio from "./pages/Portfolio/Portfolio";
import Resume from "./pages/Resume/Resume";
import "./App.css";
import { BrowserRouter, Route, Switch } from "react-router-dom";

const theme = createMuiTheme({
  typography: {
    fontFamily: "Quicksand",
  },
});

export const App = () => {
  return (
    <ThemeProvider theme={theme}>
      <Container className="top-60">
        <Grid container spacing={7}>
          <Grid item xs={12} sm={12} md={4} lg={3}>
            <Profile />
          </Grid>
          <Grid item xs>
            <BrowserRouter>
              <Header />
              <Switch>
                <Route path="/portfolio">
                  <Portfolio />
                </Route>
                <Route path="/">
                  <Resume />
                </Route>
              </Switch>
              <Footer />
            </BrowserRouter>
          </Grid>
        </Grid>
      </Container>
    </ThemeProvider>
  );
};
